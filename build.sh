#!/bin/bash

currentPath="$(pwd)"

cd ./dmsitjobs
dotnet publish -c Release -o "../_temp/dmsitjobs"

cd ${currentPath}