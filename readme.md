# README

[![pipeline status](https://gitlab.com/xlebenny/dmsitjobs/badges/main/pipeline.svg)](https://gitlab.com/xlebenny/dmsitjobs/-/commits/main)

This project using .net core 3.1,
please ensure install .net Core 3.1 SDK or newer [HERE](https://dotnet.microsoft.com/download/dotnet/3.1])

To be honest, this project haven't using design pattern,
But i can maintain SRP, OCP without to complex class,
may be you didn't agree, maybe


## Practices
- Using variable / method name to descript what i do
    - comment it used to explain why "strange", e.g: return -2
    - or split difference code block
- 2 type of method
    1. low-level by SRP
    2. high-level by OCP, combination with few low-level method, concept like design pattern 'Client' or abstract factory pattern "meal deal" example.
- 4 type of test case
    1. border case
    2. case from user, to ensure logic with user / don't make same mistake twice
    3. example case, if you want demo how to use your method (optional)
    4. normal case, ensure the logic for in the future (optional)


## Explain
`ParcelBusiness.Search`
    - this is business logic without Database connection, this is unittestable

`CsvModel`
    - your csv like a view

`SearchParcelCriteria`
    - Normal MVC practices

`XmlModel`
    - I placed `Create` in each class, 'GroupBy' logic handle their own logic 
    - Dependency: Order <- Consignment <- Parcel <- ParcelItem


## Run code
```bash
cd dmsitjobs
dotnet run
```

## Run test
```bash
cd dmsitjobs_test
dotnet test
```
