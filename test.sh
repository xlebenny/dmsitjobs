#!/bin/bash

currentPath="$(pwd)"

cd ./dmsitjobs_test
dotnet test --collect:"XPlat Code Coverage"

mkdir -p ../_temp/TestResults
mv ./TestResults/* ../_temp/TestResults
rmdir ./TestResults

cd ${currentPath}