
using System.Collections.Generic;
using System.Linq;
using dmsitjobs.models;

namespace dmsitjobs.business
{
    public class ParcelBusiness
    {
        // i love KISS (Keep it simple stupid),
        //   this can easy find and fix the bug in code review
        //  ASSUME: csv actually is database view?
        public static List<CsvModel> Search(
            // IQueryable because this should be using EF to run SQL to filter marjor records first
            //   it shouldn't get to many record to code level (because database have a index :) )
            //   PS: this should be a list from DBContext.Parcels in real case
            IQueryable<CsvModel> list,
            // MVC normal practices,
            //   but this criteria only for ONE endpoint,
            //   because each page should be difference logic & criteria
            //   if it's same, should be same page (component/dialog) and same endpoint
            SearchParcelCriteria criteria
        )
        {
            var result = list
                .Where(x => x.ParcelCode.Contains(criteria.ParcelCode))
                .ToList();
            //
            return result;
        }
    }
}