﻿using System;
using System.Linq;
using dmsitjobs.business;
using dmsitjobs.misc;
using dmsitjobs.models;

namespace dmsitjobs
{
    public class Program
    {
        static void Main(string[] args)
        {
            var csvModels = Helper.FromCsv<CsvModel>("./assets/CandidateTest_ManifestExample.csv");
            //
            Console.WriteLine("Please input ParcelCode: ");
            var searchParcelCode = Console.ReadLine();
            //
            // split to many variable because
            //   1. easy to debug, can code like a flow chart & i can print variable to confirm it's searching result wrong / to xml wrong / or something else
            //   2. can have a difference combination, maybe some day will to json?
            //   3. can unit test one by one
            var searchResult = ParcelBusiness.Search(
                csvModels.AsQueryable(),
                new SearchParcelCriteria { ParcelCode = searchParcelCode }
            );
            var result = XmlModel.Create(searchResult);
            var resultXml = result.ToXml();
            //
            Console.WriteLine(resultXml);
        }
    }
}