using System;
using System.Collections.Generic;
using System.Linq;
using CsvHelper.Configuration.Attributes;

namespace dmsitjobs.models
{
    public class CsvModel
    {
        [Name("Order No")]
        public string OrderNo { get; set; }

        [Name("Consignment No")]
        public string ConsignmentNo { get; set; }

        [Name("Parcel Code")]
        public string ParcelCode { get; set; }

        [Name("Consignee Name")]
        public string ConsigneeName { get; set; }

        [Name("Address 1")]
        public string Address1 { get; set; }

        [Name("Address 2")]
        public string Address2 { get; set; }

        [Name("City")]
        public string City { get; set; }

        [Name("State")]
        public string State { get; set; }

        [Name("Country Code")]
        public string CountryCode { get; set; }

        [Name("Item Quantity")]
        public long ItemQuantity { get; set; }

        [Name("Item Value")]
        public double ItemValue { get; set; }

        [Name("Item Weight")]
        public double ItemWeight { get; set; }

        [Name("Item Description")]
        public string ItemDescription { get; set; }

        [Name("Item Currency")]
        public string ItemCurrency { get; set; }
    }
}