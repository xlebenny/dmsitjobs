using System;
using System.Collections.Generic;
using System.Linq;

namespace dmsitjobs.models
{
    public class XmlModel
    {
        public List<Order> Orders { get; set; }

        public static XmlModel Create(List<CsvModel> list)
        {
            var result = new XmlModel
            {
                Orders = Order.Create(list),
            };
            //
            return result;
        }

        public class Order
        {
            public string No { get; set; }

            public List<Consignment> Consignments { get; set; }

            public double TotalValue
            {
                // 1. this code clear enough, can ensure by code review,
                //      so i didn't this should be writing a test case
                //
                // 2. i think this should be write in Entity, not Model,
                //      because difference "VIEW", this field should be same value
                //    this have 3 option to implement
                //    2.1 database trigger / view
                //        - pro: if some one update it by SQL, the value didn't wrong
                //               we can search total value by SQL, it have a index :)
                //        - con: can't have a preview page before user confirm
                //               it's slow (database view without materialized) 
                //    2.2 on run time, like this
                //        - pro/con: opposite with 2.1 
                //    2.3 date end generate table (one way generate, DON'T EDIT generated table)
                //        - This is useful when larger dataset with logic
                //          pro: this can easy re-generate when logic changed
                //          con: no one this should be re-generate
                //  ASSUME: same currency
                get => this.Consignments.SelectMany(x =>
                        x.Parcels.SelectMany(y =>
                            y.ParcelItems
                                .Select(z => z.Value)
                        )
                    )
                    .Sum();
            }

            //  ASSUME: same unit
            public double TotalWeight
            {
                get => this.Consignments.SelectMany(x =>
                        x.Parcels.SelectMany(y =>
                            y.ParcelItems
                                .Select(z => z.Weight)
                        )
                    )
                    .Sum();
            }

            public static List<Order> Create(List<CsvModel> list)
            {
                var result = list
                    // only `Order` class can said "what is duplicate"
                    .GroupBy(x => x.OrderNo)
                    .Select(e =>
                    {
                        var list = e.ToList();
                        var x = e.First();
                        //
                        return new Order()
                        {
                            No = x.OrderNo,
                            Consignments = Consignment.Create(list),
                        };
                    })
                    .ToList();
                return result;
            }
        }

        public class Consignment
        {
            public string No { get; set; }

            public string Name { get; set; }

            public string Address1 { get; set; }

            public string Address2 { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string CountryCode { get; set; }

            public List<Parcel> Parcels { get; set; }

            public static List<Consignment> Create(List<CsvModel> list)
            {
                var result = list
                    .GroupBy(x => x.ConsignmentNo)
                    .Select(e =>
                    {
                        var list = e.ToList();
                        var x = e.First();
                        //
                        return new Consignment()
                        {
                            No = x.ConsignmentNo,
                            Name = x.ConsigneeName,
                            Address1 = x.Address1,
                            Address2 = x.Address2,
                            City = x.City,
                            State = x.State,
                            CountryCode = x.CountryCode,
                            Parcels = Parcel.Create(list),
                        };
                    })
                    .ToList();
                return result;
            }
        }


        public class Parcel
        {
            public string Code { get; set; }

            public List<ParcelItem> ParcelItems { get; set; }

            public static List<Parcel> Create(List<CsvModel> list)
            {
                var result = list
                    .GroupBy(x => x.ParcelCode)
                    .Select(e =>
                    {
                        var list = e.ToList();
                        var x = e.First();
                        //
                        return new Parcel()
                        {
                            Code = x.ParcelCode,
                            // this is case, no example is 1 Parcel : many items
                            ParcelItems = ParcelItem.Create(list),
                        };
                    })
                    .ToList();
                return result;
            }
        }

        public class ParcelItem
        {
            public long Quantity { get; set; }

            public double Value { get; set; }

            public double Weight { get; set; }

            public string Description { get; set; }

            private string _currency { get; set; }

            public string Currency
            {
                get => _currency;
                set => _currency = (value == null || value.Trim() == "") ? "GBP" : value;
            }

            public static List<ParcelItem> Create(List<CsvModel> list)
            {
                // no check duplicate logic
                var result = list
                    .Select(x => new ParcelItem()
                    {
                        Quantity = x.ItemQuantity,
                        Value = x.ItemValue,
                        Weight = x.ItemWeight,
                        Description = x.ItemDescription,
                        Currency = x.ItemCurrency,
                    })
                    .ToList();
                return result;
            }
        }
    }
}