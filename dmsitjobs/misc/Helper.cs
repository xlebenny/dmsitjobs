using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Xml.Serialization;
using CsvHelper;
using CsvHelper.Configuration;

namespace dmsitjobs.misc
{
    public static class Helper
    {
        public static List<T> FromCsv<T>(string filePath)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                TrimOptions = TrimOptions.Trim,
            };

            return FromCsv<T>(filePath, config);
        }

        public static List<T> FromCsv<T>(string filePath, CsvConfiguration config)
        {
            using (var reader = new StreamReader(filePath))
            {
                using (var csv = new CsvReader(reader, config))
                {
                    var records = csv.GetRecords<T>();
                    //
                    // i hate IEnumerable, because can't .Where
                    return records.ToList();
                }
            }
        }

        public static string ToJson(object obj, bool indented = true)
        {
            // ref https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-how-to?pivots=dotnet-6-0
            var options = new JsonSerializerOptions { WriteIndented = indented };
            var jsonString = JsonSerializer.Serialize(obj, options);
            return jsonString;
        }

        public static string ToXml(object obj)
        {
            // ref https://docs.microsoft.com/en-us/troubleshoot/dotnet/csharp/serialize-object-xml
            var serializer = new XmlSerializer(obj.GetType());
            using (var stream = new MemoryStream())
            {
                serializer.Serialize(stream, obj);
                //
                var result = Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Length);
                return result;
            }
        }
    }

    public static class HelperExtension
    {
        public static string ToJson(this object obj, bool indented = true)
        {
            return Helper.ToJson(obj, indented);
        }

        public static string ToXml(this object obj)
        {
            return Helper.ToXml(obj);
        }
    }
}