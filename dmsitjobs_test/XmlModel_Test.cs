using System;
using System.Collections.Generic;
using System.Linq;
using dmsitjobs.misc;
using dmsitjobs.models;
using Xunit;
using Xunit.Abstractions;

namespace dmsitjobs.business
{
    public class XmlModel_Test
    {
        private readonly ITestOutputHelper Output;

        private readonly List<CsvModel> Models;

        public XmlModel_Test(ITestOutputHelper output)
        {
            Output = output;
            //
            // ASSUME: Helper.FromCsv is work
            Models = Helper.FromCsv<CsvModel>("./assets/OrderView.csv");
        }

        [Fact]
        public void NormalCase1()
        {
            var result = XmlModel.Create(new List<CsvModel>() {
                Models[0],
                Models[1],
            });

            // Order
            Assert.Equal(Models[0].OrderNo, result.Orders[0].No);
            Assert.Equal(Models.Take(2).Select(x => x.ItemValue).Sum(), result.Orders[0].TotalValue);
            Assert.Equal(Models.Take(2).Select(x => x.ItemWeight).Sum(), result.Orders[0].TotalWeight);
            // Consignments
            Assert.Single(result.Orders[0].Consignments);
            Assert.Equal(Models[0].Address1, result.Orders[0].Consignments[0].Address1);
            Assert.Equal(Models[0].Address2, result.Orders[0].Consignments[0].Address2);
            Assert.Equal(Models[0].City, result.Orders[0].Consignments[0].City);
            Assert.Equal(Models[0].CountryCode, result.Orders[0].Consignments[0].CountryCode);
            Assert.Equal(Models[0].ConsigneeName, result.Orders[0].Consignments[0].Name);
            Assert.Equal(Models[0].ConsignmentNo, result.Orders[0].Consignments[0].No);
            Assert.Equal(Models[0].State, result.Orders[0].Consignments[0].State);
            // Parcels
            Assert.Equal(2, result.Orders[0].Consignments[0].Parcels.Count);
            Assert.Equal(Models[0].ParcelCode, result.Orders[0].Consignments[0].Parcels[0].Code);
            Assert.Equal(Models[1].ParcelCode, result.Orders[0].Consignments[0].Parcels[1].Code);
            // Parcels0 - ParcelItems
            Assert.Single(result.Orders[0].Consignments[0].Parcels[0].ParcelItems);
            Assert.Equal("GBP", result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Currency);
            Assert.Equal(Models[0].ItemDescription, result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Description);
            Assert.Equal(Models[0].ItemQuantity, result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Quantity);
            Assert.Equal(Models[0].ItemValue, result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Value);
            Assert.Equal(Models[0].ItemWeight, result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Weight);
            // Parcels1 - ParcelItems
            Assert.Single(result.Orders[0].Consignments[0].Parcels[0].ParcelItems);
            Assert.Equal("GBP", result.Orders[0].Consignments[0].Parcels[1].ParcelItems[0].Currency);
            Assert.Equal(Models[1].ItemDescription, result.Orders[0].Consignments[0].Parcels[1].ParcelItems[0].Description);
            Assert.Equal(Models[1].ItemQuantity, result.Orders[0].Consignments[0].Parcels[1].ParcelItems[0].Quantity);
            Assert.Equal(Models[1].ItemValue, result.Orders[0].Consignments[0].Parcels[1].ParcelItems[0].Value);
            Assert.Equal(Models[1].ItemWeight, result.Orders[0].Consignments[0].Parcels[1].ParcelItems[0].Weight);
        }


        [Fact]
        public void NonEmptyCurrency()
        {
            var result = XmlModel.Create(new List<CsvModel>() {
                Models[4],
            });
            Assert.Same(Models[4].ItemCurrency.Trim(), result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Currency);
        }

        [Fact]
        public void EmptyCurrency()
        {
            var result = XmlModel.Create(new List<CsvModel>() {
                Models[3],
            });
            Assert.Same("GBP", result.Orders[0].Consignments[0].Parcels[0].ParcelItems[0].Currency);
        }
    }
}