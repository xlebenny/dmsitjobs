using System;
using System.Collections.Generic;
using System.Linq;
using dmsitjobs.misc;
using dmsitjobs.models;
using Xunit;
using Xunit.Abstractions;

namespace dmsitjobs.business
{
    public class ParcelBusiness_Test
    {
        private readonly ITestOutputHelper Output;

        private readonly List<CsvModel> Models;

        public ParcelBusiness_Test(ITestOutputHelper output)
        {
            Output = output;
            //
            // ASSUME: Helper.FromCsv is work
            Models = Helper.FromCsv<CsvModel>("./assets/CandidateTest_ManifestExample.csv");
        }

        [Fact]
        public void NormalCase1()
        {
            //
            var result = Search(new SearchParcelCriteria
            {
                ParcelCode = ""
            });
            //
            Assert.Equal(Models.Count, result.Count);
        }

        [Fact]
        public void NormalCase2()
        {
            //
            var result = Search(new SearchParcelCriteria
            {
                ParcelCode = "2"
            });
            //
            Assert.Single(result);
            Assert.Equal("PARC002", result[0].ParcelCode);
        }


        [Fact]
        public void NormalCase3()
        {
            //
            var result = Search(new SearchParcelCriteria
            {
                ParcelCode = "9"
            });
            //
            Assert.Empty(result);
        }

        private List<CsvModel> Search(SearchParcelCriteria criteria)
        {
            var result = ParcelBusiness.Search(
                Models.AsQueryable(),
                criteria
            );
            return result;
        }
    }
}